#include "asm_operations.h"
#include "data_structures.h"

// load: B (The register) <- '8 bit value'
void ld_b_n(unsigned char n)
{
    //registers.b = n;
}

// load: BC (The register) <- '16 bit value'
void ld_bc_nn(unsigned short nn)
{
    //registers.bc = nn;
}