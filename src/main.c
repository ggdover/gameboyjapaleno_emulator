#include "asm_operations.h"
#include "data_structures.h"

void parse(unsigned short opcode)
{
    if (opcode >= g_instr_count)
    {
        printf("ERR opcode: %u, not in lookup list. out_of_range (g_instr_count = %zu)\n", opcode, g_instr_count);
        return;
    }

    struct instruction ins = instructions[ opcode ];
    if (ins.execute == NULL)
    {
        printf("ERR instruction: '%s' (opcode=%u) is not implemented (execute() == NULL)\n", ins.disassembly, opcode);
    }
    else
    {
        ins.execute();
    }
}

int main(void)
{
    unsigned short bla = registers.bc;
    
    int status;

    printf("Hello world\n");

    /***********************************************/
    // Initial error checks
    if (g_instr_count >= g_static_count)
    {
        printf("ERR! instr_count is too large, needs to be less than static_count.\n(instr_count = %zu, static_count = %zu)\n", g_instr_count, g_static_count);
        return 0;
    }
    /***********************************************/
    // Main program

    parse(0x0);
    parse(0x01);
    parse(0x02);
 
    return 0;
}