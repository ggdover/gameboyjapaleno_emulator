#include "data_structures.h"

struct registers_type registers;

size_t g_static_count = sizeof(instructions)/sizeof(instructions[0]);
size_t const g_instr_count = 3;
const struct instruction instructions[256] = {
	{ "NOP", 0, NULL},                  // 0x00
	{ "LD BC, 0x%04X", 2, NULL },       // 0x01
	{ "LD (BC), A", 0, NULL }           // 0x02
};
