### Summary

Goal with this project is to complete a fully workable gameboy emulator, created with full-fucker help from guides.

The point of this project is not to learn about how a gameboy emulator works, even though it comes along for free, the goal is to have a fully working gameboy emulator up and running as soon as possible using any guides or other help necessary.

Overarching this is a side-project/knowledge building approach I want to try out, where I complete the project in a type of brute-force mindless, without trying to learn anything but just completing the objective/practical tasks as fast as possible and then backtrack all the things I want to learn more indepth about. Perhaps even write the whole code from scratch again and the this time with alot less help from guides and other reading material.
This will hopefully minimize the amount of time I typically spend on unfocused and unnecessary research before I start producing code for a side project, and make all the knowledge I wanna do much more focused and time-efficent.

If after trying this out and I feel like it works well, then I will apply it for any future side-projects.