
/**
 * Terminology
 * ld = Load
 * n = any 8 bit number
 * nn = any 16 bit number
 **/

// 0x02
void ld_bc_nn(unsigned short nn);

// 0x06
void ld_b_n(unsigned char n);
