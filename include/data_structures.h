#include <stdio.h>

struct registers_type {
    struct {
        union {
            struct {
                unsigned char f; // 8 bit register
                unsigned char a; // 8 bit register
            };
            unsigned short af; // 16 bit (8+8 bit)
        };
    };

    struct {
        union {
            struct {
                unsigned char c; // 8 bit register
                unsigned char b; // 8 bit register
            };
            unsigned short bc; // 16 bit (8+8)
        };
    };

    struct {
        union {
            struct {
                unsigned char e; // 8 bit register
                unsigned char d; // 8 bit register
            };
            unsigned short de; // 16 bit (8+8)
        };
    };

    struct {
        union {
            struct {
                unsigned char l; // 8 bit register
                unsigned char h; // 8 bit register
            };
            unsigned short hl; // 16 bit (8+8)
        };
    };

    unsigned short sp; // 16 bit register, stack pointer
    unsigned short pc; // 16 bit register, program counter/pointer
} extern registers;

struct instruction {
	char *disassembly; // Assembly code (Disassembly = Machine/Object code -> Assembly)
	unsigned char operandLength; // Number of bytes (0 = 1 byte, 1 = 2 bytes ... etc.)
	void (*execute)(void); // Pointer to function reflecting this instruction
	//unsigned char ticks; // Number of cpu cycles
} extern const instructions[256];

// All global variables
extern size_t g_static_count;
extern size_t const g_instr_count;